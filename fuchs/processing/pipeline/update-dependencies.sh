set -e

./develop.sh \
  there-can-be-uncommited-change \
  yes-internet-is-allowed \
  run \
  yes-do-dependency-updates
