set -e

./develop.sh \
  "" \
  no-internet \
  no-run \
  no-dependency-update


# ./develop.sh \
#   everything-is-commited \
#   no-internet \
#   no-run \
#   no-dependency-update

runhaskell \
  --ghc-arg="-main-is" \
  --ghc-arg="stonefish_profiling" \
  --ghc-arg="-iweb-embedding/web-embedding/processing/source" \
  Profiling.hs
