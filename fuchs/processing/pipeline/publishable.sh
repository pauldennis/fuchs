set -e

./develop.sh \
  everything-is-commited \
  no-internet \
  run \
  no-dependency-update
