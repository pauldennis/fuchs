# string representation

[[_TOC_]]

## alphabet

The string representation uses the following alphabet:

```
abcdefghijklmnopqrstuvwxyz
ABCDEFGHIJKLMNOPQRSTUVWXYZ
↦ (){}=↵
```

## runtime states

```
literal ::= "data₀" | ... | "data₄₂₉₄₉₆₇₂₉₅"
constructor ::= "C₁" | "C₂" | ...
rule ::= "R₁" | "R₂" | ...
variable ::= "x₁" | "x₂" | ...

term
  ::= literal                   # 32-bits machine word
  | variable                    # variable occurence
  | constructor term ... term   # construction from terms
  | rule term ... term          # rule on terms
  | variable ↦ term            # lambda abstraction
  | term term                   # application
  | {term term}                 # superposition

erasure ::= * = term

duplication ::= variable variable = term

runtimeState ::=
  erasure↵
  ...↵
  erasure↵
  ↵
  duplication↵
  ...↵
  duplication↵
  ↵
  term↵
  ...↵
  term↵

```

## rule instructions

```
instruction
  ::= variable <- term          # variable substitution
  | erasure                     # new erasure
  | duplication                 # new duplication
  | term                        # new root for embedder

rule ::=
  term↵                         #
  "--------------------"↵
  instruction↵
  ...↵
  instruction↵
```

## rules of inference

All variables before the line are pattern matched. All used variable after the line are unused and fresh variables.

### erase literal
```
* = dataₙ
--------------------
```

### erase constructor
```
* = C x₁ ... xₙ
--------------------
* = x₁
...
* = xₙ
```

### erase twin
```
* = l
l r = b
--------------------
r <- b
```
```
* = r
l r = b
--------------------
l <- b
```

### application of lambda
```
(x ↦ b) a
--------------------
x <- a
b
```

### application of superposition
```
{l r}ᵢ = a
--------------------
aₗ aᵣ =ᵢ a
{(l aₗ) (r aᵣ)}
```


### intrinsic rule of addition
```
+ dataₙ dataₘ
--------------------
dataₙ₊ₘ
```

### rule that is If
```
If True then else
--------------------
* = True
* = else
then
```
```
If False then else
--------------------
* = False
* = then
else
```

### rule of superposition
```
R xs {a b}ᵢ ys
--------------------
xsₗ xsᵣ =ᵢ xs
ysₗ ysᵣ =ᵢ ys
* = then
{(R xsₗ a ysₗ) (R xsᵣ b ysᵣ)}
```

### twin of literal
```
l r = dataₙ
--------------------
l <- dataₙ
r <- dataₙ
```

### twin of constructor
```
l r = C x₁ ... xₙ
--------------------
l <- C l₁ ... lₙ
r <- C r₁ ... rₙ
l₁ r₁ = x₁
...
lₙ rₙ = xₙ
```

### twin of lambda
```
l r = (x ↦ b)
--------------------
l <- (xₗ ↦ bₗ)
r <- (xᵣ ↦ bᵣ)
x <- {xₗ xᵣ}ᵢ
bₗ bᵣ =ᵢ
```

### twin of superposition easy variant
```
l r =ᵢ {xₗ xᵣ}ᵢ
--------------------
l <- xₗ
r <- xᵣ
```

### twin of superposition swap variant
```
i≠j
l r =ᵢ {x y}ⱼ
--------------------
l <- {lₓ lᵧ}ⱼ
r <- {rₓ rᵧ}ⱼ
lₓ rₓ =ᵢ u
lᵧ rᵧ =ᵢ v
```



### disintegrate constructor
```
C x₁ ... xₙ
--------------------
C
x₁
...
xₙ
```
