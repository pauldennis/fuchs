# the stonefish runtime

[https://github.com/HigherOrderCO/](https://github.com/HigherOrderCO/):
- [How.md](https://github.com/high-cloud/HVM1/blob/1336bb326db40d71eb09b306ddeeb8800f0c1f16/HOW.md)
- [runtime.rs](https://github.com/high-cloud/HVM1/blob/1336bb326db40d71eb09b306ddeeb8800f0c1f16/src/runtime.rs)
- [runtime.c](https://github.com/high-cloud/HVM1/blob/1336bb326db40d71eb09b306ddeeb8800f0c1f16/src/runtime.c)
- [Interaction nets](https://en.wikipedia.org/w/index.php?title=Interaction_nets&oldid=1247887789)

# table of contentss

# three representations

## string representation
 - `abcdefghijklmnopqrstuvwxyzauABCDEFGHIJKLMNOPQRSTUVWXYZλ (){}=↵`
 - `(`, `)`


### examples for string terms:

literal data: 32-bits of data
```
data₀
data₁
data₂
data₉₇₆
data₄₂₉₄₉₆₇₂₉₅
```
variables: lowercase identifier
```
x
y
a
b
l
x₁
xₙ
xₗ
xᵣ
argument
body
elseBranch
```
constructors: uppercase identifier
```
C x₁ ... xₙ
D
True
False
BinaryTree subtreeₗ data₇ subtreeᵣ
```
rules: uppercase identifiers
```
R x₁ x₂
Multiplicate data₇ data₇
If True then else
```
lambda abstractions: math like maps to expressions
```
x ↦ x
x ↦ Negate x
x ↦ Multiplicate xₗ xᵣ
```
application: haskell like application flavour, but only one argument
```
f x
f (f x)
```
superposition: set like notation, but order matters though
```
{l r}
{u v}
{(x ↦ x) (x ↦ Negate x)}
{{a b} {c d}}
```
erasure: expressions not referenced anymore
```
* = data₉₇₆
* = xₗ
* = True
* = BinaryTree xₗ data₇ (BinaryTree Nil data₇ (BinaryTree Nil data₇ Nil))
```
duplication: an expression referenced twice
```
l r = body
xₗ xᵣ = x
```

## net representation

![agent](diagrams/rendered/abstract.png){width=33%}


## heap representation

# agent: constructor

![hanger](diagrams/rendered/constructor.png){width=33%}


# agent: literal

![box](diagrams/rendered/literal.png){width=33%}

# example: binary search tree
![box](diagrams/rendered/balanced-binary-search-tree.png){width=66%}

# agent: erasure

![dead-end](diagrams/rendered/erasure.png){width=33%}

# rule of inference: erase literal
![erase literal](diagrams/rendered/erase-literal.png){width=66%}

# rule of inference: erase constructor

![erase constructor](diagrams/rendered/erase-constructor.png){width=66%}

# example: erase tree

![erase tree](diagrams/rendered/erase-tree.png){width=66%}

# agent: rule
![hacksaw](diagrams/rendered/rule.png){width=33%}

# intrinsic rule: addition and comparison

![intrinsic rule of addition](diagrams/rendered/addition.png){width=66%}
![intrinsic rule of comparison](diagrams/rendered/comparison.png){width=66%}
![intrinsic rule of minnimum](diagrams/rendered/minnimum.png){width=66%}
![intrinsic rule of decrementation](diagrams/rendered/Decrement.png){width=66%}

# example: minnimum tree
![minimum tree](diagrams/rendered/minimum-tree.png){width=33%}

# example: custom rule: If
![rule that is If](diagrams/rendered/If.png){width=66%}

# example: custom rule: Fibonacci
![three rules for fibonacci](diagrams/rendered/Fibonacci.png){width=66%}

# output: disintegrate constructor

![disintegrate constructor](diagrams/rendered/disintegrate-constructor.png){width=66%}

# agent: duplication
![slider](diagrams/rendered/duplication.png){width=33%}

# rule of inference: twin of literal
![twin of literal](diagrams/rendered/twin-of-literal.png){width=66%}

# example: Numbers
![Numbers](diagrams/rendered/Numbers.png){width=66%}

# rule of inference: twin of constructor
![twin of constructor](diagrams/rendered/twin-of-constructor.png){width=66%}

# rule of inference: erase twin
![erase twin](diagrams/rendered/erase-twin.png){width=66%}

# agent: lambda
![heart](diagrams/rendered/lambda.png){width=33%}

# example: identity
![identity](diagrams/rendered/identity.png){width=33%}


# agent: application
![sickle](diagrams/rendered/application.png){width=33%}

# example: custom rule: Compose
![rule that is If](diagrams/rendered/Compose.png){width=66%}

# rule of inference: application of lambda
![application of lambda](diagrams/rendered/application-of-lambda.png){width=66%}

# example: application of identity on literal
![application of identity on literal](diagrams/rendered/apply-identity-to-literal.png){width=33%}

# example: composition of two identities
![composition of two identities](diagrams/rendered/composition-of-two-identities.png){width=33%}

# agent: superposition
![junction](diagrams/rendered/superposition.png){width=33%}

# rule of inference: twin of lambda
![twin of lambda](diagrams/rendered/twin-of-lambda.png){width=66%}

# rule of inference: application of superposition

 - `{f g} a` ↷ `{(f a) (g a)}` (implicit)

 - `(f {} g) · a` ↷ `(f · a) {} (g · a)`

![application of superposition](diagrams/rendered/application-of-superposition.png){width=66%}

# rule of inference: rule on superposition
![rule of superposition](diagrams/rendered/rule-of-superposition.png){width=66%}

# rule of inference: twin of superposition easy variant
![twin of superposition easy](diagrams/rendered/twin-of-superposition-easy.png){width=66%}

# rule of inference: twin of superposition swap variant
![twin of superposition swap](diagrams/rendered/twin-of-superposition-swap.png){width=66%}

# single threaded deterministic evaluation

## weak-head-mormal-form
 - redex (reducible expression)

 - not in whnf
```
application
duplication
rule
```
in whnf
```
literal
constructor
lambda
superposition
```

nicht vorgekommen:
```
superposition
lambda variable
```

## algorithm

  - `reduce`
  - nicht immer wachsen

  input: root
  output: new root, but same wire


In case the root agents is:
  - a literal: return
  - a constructor: return
  - a lambda: return
  - a superposition: return
  - an application:
    * apply application rule
    * `reduce` result again
  - a duplication:
    * apply on of the twin-of rules
    * `reduce` result again
  - a rule:
    * reduce all strict arguments from left to right
      * in case argument is superposition apply relevant rule; return
    * in case all strict arguments evaluated: apply the custom rule (perhaps with pattern matching)
    * `reduce` result again
  - a lambda variable
    * unreachable

With these ingredients the embedder can interact with the runtime state, by:
  - input: linking new agents
  - output: disintegrating constructors
  - composing: combining two roots with a new application node
  - drive: calling reduce to drive computation forward
