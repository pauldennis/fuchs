set -e

pushd rendered
  rm *
popd

convert_diagram()
{
  inkscape $1/$2.svg --export-filename=rendered/$2.png
}


convert_diagram agents abstract
convert_diagram agents application
convert_diagram agents application
convert_diagram agents constructor
convert_diagram agents duplication
convert_diagram agents erasure
convert_diagram agents lambda
convert_diagram agents literal
convert_diagram agents rule
convert_diagram agents superposition

convert_diagram rules-of-inference application-of-lambda
convert_diagram rules-of-inference application-of-superposition
convert_diagram rules-of-inference disintegrate-constructor
convert_diagram rules-of-inference erase-constructor
convert_diagram rules-of-inference erase-literal
convert_diagram rules-of-inference erase-twin
convert_diagram rules-of-inference twin-of-literal
convert_diagram rules-of-inference rule-of-superposition
convert_diagram rules-of-inference twin-of-constructor
convert_diagram rules-of-inference twin-of-lambda
convert_diagram rules-of-inference twin-of-superposition-easy
convert_diagram rules-of-inference twin-of-superposition-swap

convert_diagram intrinsic-rules addition
convert_diagram intrinsic-rules comparison
convert_diagram intrinsic-rules minnimum
convert_diagram intrinsic-rules Decrement

convert_diagram custom-rules If
convert_diagram custom-rules Compose
convert_diagram custom-rules Fibonacci
convert_diagram custom-rules Numbers

convert_diagram examples balanced-binary-search-tree
convert_diagram examples identity
convert_diagram examples erase-tree
convert_diagram examples minimum-tree
convert_diagram examples apply-identity-to-literal
convert_diagram examples composition-of-two-identities
