# the stonefish runtime

These pages explains the underlying computational model of a runtime that is called stonefish and its implementation. The underlying model is mainly taken from the concepts and ideas from the following files found in this repository and some other sources: [https://github.com/HigherOrderCO/](https://github.com/HigherOrderCO/):
- [How.md](https://github.com/high-cloud/HVM1/blob/1336bb326db40d71eb09b306ddeeb8800f0c1f16/HOW.md)
- [runtime.rs](https://github.com/high-cloud/HVM1/blob/1336bb326db40d71eb09b306ddeeb8800f0c1f16/src/runtime.rs)
- [runtime.c](https://github.com/high-cloud/HVM1/blob/1336bb326db40d71eb09b306ddeeb8800f0c1f16/src/runtime.c)
- [Interaction nets](https://en.wikipedia.org/w/index.php?title=Interaction_nets&oldid=1247887789)

# table of contentss

[[_TOC_]]

# three representations
From a semantic perspective there are three interchangeable representations for the runtime state of a stonefish instance. Some rules of inference are easier to understand in string representation. Some are easier in net representation.

## string representation
In the string representation the state is represented by a string over some alphabet. The used alphabet is `abcdefghijklmnopqrstuvwxyzauABCDEFGHIJKLMNOPQRSTUVWXYZλ (){}=↵` which is some Unicode subset. The symbols are typed in some typewriter font in order to emphasize that they are terminal symbols. There are certain rules that describe what valid strings are. For example there is the property that every `(` has an associated `)` after. The rules of inference describe what string is to be evaluated to what new string. The string representation for the stonefish runtime is a variation of the classical lambda calculus. A more formal definition of the set of all valid runtime states is given [here](./string-representation.md)

### examples for string terms:

literal data: 32-bits of data
```
data₀
data₁
data₂
data₉₇₆
data₄₂₉₄₉₆₇₂₉₅
```
variables: lowercase identifier
```
x
y
a
b
l
x₁
xₙ
xₗ
xᵣ
argument
body
elseBranch
```
constructors: uppercase identifier
```
C x₁ ... xₙ
D
True
False
BinaryTree subtreeₗ data₇ subtreeᵣ
```
rules: uppercase identifiers
```
R x₁ x₂
Multiplicate data₇ data₇
If True then else
```
lambda abstractions: math like maps to expressions
```
x ↦ x
x ↦ Negate x
x ↦ Multiplicate xₗ xᵣ
```
application: haskell like application flavour, but only one argument
```
f x
f (f x)
```
superposition: set like notation, but order matters though
```
{l r}
{u v}
{(x ↦ x) (x ↦ Negate x)}
{{a b} {c d}}
```
erasure: expressions not referenced anymore
```
* = data₉₇₆
* = xₗ
* = True
* = BinaryTree xₗ data₇ (BinaryTree Nil data₇ (BinaryTree Nil data₇ Nil))
```
duplication: an expression referenced twice
```
l r = body
xₗ xᵣ = x
```


In the classical representation of the lambda calculus there are unnatural constraints while working with the calculus. There are things like: Before a variable substitution can be done one has to make sure that the to be substituted variable is unbound. Or sometimes the variable has to be renamed or similar details. For the rules there are some side conditions necessary to ensure that new variables are not accidentally bound by some outer lambda. This could be implemented by incrementing a 64-bit counter for example.

## net representation
The net representation is designed for a graphical visualization of the runtime state. The net encoding illustrates two desirable properties.
  - physicality (by having a graph one can draw)
  - linearity (by representing variables by wires)
  - concurrency (by only working on subgraphs)

![agent](diagrams/rendered/abstract.png){width=33%}

In the net representation there are agents that have a number of ordered ports. The net consists of some agents that are connected by their ports via wires.

Subnets are visualized by some net with root wires that point to some unknown parent. The parent is some other agent or the embedder itself. Pattern matched sub children nets are wires going down.

There are no variable names. Certain wires are used to represent variables. The variable is represented by the bounding lambda and its occurrence.

The nets can be thought of as some mobile (kinetic sculpture) hanging off an edge. One can pull a root and disintegrate the agent by removing the children from the agent to gain new roots. The roots can be reassembled to some new mobile.

For valid runtime states all wires are always connected to something. Also every agent can be reached by a root. Evaluations change local subnets of the whole net. The rules of inference are like a surgeon that operates on an organism: The surgeon only operates inside an incision and in the end every tract gotta be be connected to something.

Many rules have pairs of a left and right wire. The order is important. The wires are always canonically sorted as one would have guessed.

## heap representation
In actual heap representation that is used for the actual implementation of the stonefish runtime there are nodes instead of agents. They resemble the agents but there are differences. The wires are modelled by tagged pointers called fingers. Some wires representations are modelled bidirectional. Most are unidirectional.

     node          | into  | outo  | bidirectional
    ---------------| ----- | ----- | -------------
     constructor   | 1     | 0..N  |
     erasure       |       | 1     |
     duplication   |       | 1     | 2
     lambda        | 1     | 1     | 1
     application   | 1     | 2     |
     superposition | 1     | 2     |
     rule          | 1     | 0..N  |
     literal       | 1     |       |


# agent: constructor
Constructors fulfill a similar goal as structs in the C programming language. Constructors are an important ingredient for moddeling [Algebraic data types](https://en.wikipedia.org/wiki/Algebraic_data_type).

![hanger](diagrams/rendered/constructor.png){width=33%}

A constructor has a name `C`. Every constructor has an arity `n`. The constructor always has `n` many children. Constructors behave like a function that is never evaluated. The evaluation of a constructor is the constructor itself.

# agent: literal
Here literals are small native machine words of 32-bits.

![box](diagrams/rendered/literal.png){width=33%}

A literal agent does not have any downward ports.

Literal can be represented efficiently. This can be called "unboxed". In the stonefish runtime it might happen that the embedder holds the data itself and the runtime store does not have a hold on the value itself: The embedder holds on roots represented by fingers. A finger may represent the literal itself.

# example: binary search tree
![box](diagrams/rendered/balanced-binary-search-tree.png){width=66%}

# agent: erasure
Unused data needs to be deleted in order to not leak memory from the implementation.

![dead-end](diagrams/rendered/erasure.png){width=33%}

The erasure agent is rootless. In the actual implementation it does not exist as a node. In case something needs to get erased evaluation is stopped until everything is erased recursively.

# rule of inference: erase literal
![erase literal](diagrams/rendered/erase-literal.png){width=66%}

The erasure of some literal data is easy. The data just gets dropped. In the implementation this is realized by a variable on the stack that goes out of scope. The right side of the rule is empty.

# rule of inference: erase constructor
The erasure of a constructor is easy. The hull is dropped and all children are to erased recursively. The information of the constructor identification and the order and existence of the children are lost.

![erase constructor](diagrams/rendered/erase-constructor.png){width=66%}

In case `n` is zero the right side is empty.

# example: erase tree

![erase tree](diagrams/rendered/erase-tree.png){width=66%}

# agent: rule
![hacksaw](diagrams/rendered/rule.png){width=33%}

Similar to constructor agents but when rule agents are evaluated they evaluate their children and do some pattern matching on the first child. Then they get replaced by some usually small net (depending on the constructor matched identification). This minninet is freshly copied into the runtime state. After that this replacement is evaluated again. One could do by only using lambda nodes and duplicating huge terms but this would be unfavourable for performance reasons. It is faster to just copy a bunch of new agents every time a rule is evaluated. Rules are the counterpart for constructors. Rule agents eliminate constructor agents by pattern matching them.

# intrinsic rule: addition and comparison
intrinsic rules are used by the stonefish implementation to forward the computers [ALU](https://en.wikipedia.org/w/index.php?title=Arithmetic_logic_unit&oldid=1247247120) capabilities to the user program. This is a crucial feature for practical performance.

![intrinsic rule of addition](diagrams/rendered/addition.png){width=66%}
![intrinsic rule of comparison](diagrams/rendered/comparison.png){width=66%}
![intrinsic rule of minnimum](diagrams/rendered/minnimum.png){width=66%}
![intrinsic rule of decrementation](diagrams/rendered/Decrement.png){width=66%}

These are only example rules. There are a lot more rules but only finite many.

# example: minnimum tree
![minimum tree](diagrams/rendered/minimum-tree.png){width=33%}

This example illustrates the concurrency of nets. The first two rule applications could be done in any order or in parallel.

# example: custom rule: If
The universally known `If` rule can be implemented by the user.

![rule that is If](diagrams/rendered/If.png){width=66%}

This is only the rule for encountering a True in the predicate.

There is a False rule counterpart that discards `then` and returns `else` instead.

# example: custom rule: Fibonacci
![three rules for fibonacci](diagrams/rendered/Fibonacci.png){width=66%}

# output: disintegrate constructor
Disintegration is pattern matching for the embedder. The embedder can use this operation to retrieve output from the computation. The constructor identification and the children are not lost but available to the embedder.

![disintegrate constructor](diagrams/rendered/disintegrate-constructor.png){width=66%}

The `n` is redundant because it is the length of the list of new roots.

For input the embedder might also introduce new agents for combining the new roots again.

# agent: duplication
Every variable and therefore every expression shall be used exactly once. This property is a very natural property in the representation of nets. A variable wire cannot fray. This is the same as a variable cannot occur more than once. A variable wire must be connected to some port. This is the same as that every variable does occur at least once.

But often one needs to duplicate some value. For example the square function `λx(x*x)` that squares its input needs to copy its argument. For this purpose the duplication agent allows to clone an expression into a left twin and a right twin `λx(let l r = x in l*r)`.

![slider](diagrams/rendered/duplication.png){width=33%}

There are two twin root ports. They have an order and have left/rightness associated with them. Then there is a duplication body port downwards.

The rules of inference are designed in a way that the duplicated value is shallow copied layer by layer on demand. When evaluating a twin the duplication body gets (shallowly) inlined. Only the outmost agent is actual copied. The rest is reused and prepared for duplication at a later evaluation step.

# rule of inference: twin of literal

![twin of literal](diagrams/rendered/twin-of-literal.png){width=66%}

Literals get cloned completely. If the literal is a pointer to some mutable allocated memory then the whole array gets copied now.

# example: Numbers
It is possible to represent infinite lists.

![Numbers](diagrams/rendered/Numbers.png){width=66%}

# rule of inference: twin of constructor
Duplicating a constructor is easy to understand in the net representation.

![twin of constructor](diagrams/rendered/twin-of-constructor.png){width=66%}

The constructor agent is used twice. Therefore it is behind a duplication agent. After the rule there are two constructor agents. The children are not copied. But they have to be used twice for the two new constructors. Therefore there is a new duplication node for every child.

The left/rightness of the children is crucial. The left constructor has to have left twins as children.

# rule of inference: erase twin

A twin might not be needed anymore. For example it might get dropped as an if-then-else arm. The duplication Is not needed anymore as there is only one copy needed.

![erase twin](diagrams/rendered/erase-twin.png){width=66%}

The duplication node gets dropped. The former twin gets replaced by the duplication body directly.

There is a symmetric rule for the erasure of a left twin.

# agent: lambda
Lambda agents are like classical lambdas in a lambda calculus. A term is "abstracted" by binding a free variable. With the lambda calculus presented here there is the special property that every bound variable has to occur exactly once. This one occurrence is sophisticatedly implemented by a variable occurrence wire into the lambda body expression.

![heart](diagrams/rendered/lambda.png){width=33%}

There is a port for the lambda body. The other port is for a variable leash wire to the variable occurrence.

The variable leash points into the body net. This property has a corresponding requirement for the string representation: The lambda body string contains the variable.

# example: identity

![identity](diagrams/rendered/identity.png){width=33%}

The simplest form of a lambda is the identity lambda. Despite its simplicity it needs special consideration in the implementation.

# agent: application
An application agent allows to replace a variable of a lambda agent with some argument term. Application agents are the counterpart of lambda agents. An application agent eliminates a lambda agent by replacing the abstract bound variable with some concrete argument term.

![sickle](diagrams/rendered/application.png){width=33%}

The `f` port wires to the tool that is applied. The `x` port wires to the argument the tool is applied to.

# example: custom rule: Compose

![rule that is If](diagrams/rendered/Compose.png){width=66%}

# rule of inference: application of lambda

Application of a lambda is the counter part of a lambda "abstraction".

![application of lambda](diagrams/rendered/application-of-lambda.png){width=66%}

Both agents get dropped. The only variable of the lambda gets dropped.

# example: application of identity on literal
![application of identity on literal](diagrams/rendered/apply-identity-to-literal.png){width=33%}


# example: composition of two identities
![composition of two identities](diagrams/rendered/composition-of-two-identities.png){width=33%}


# agent: superposition
How to do shallow copies of lambda agents? First lets say that the lambda body is to be duplicated later. The lambda itself gets shallow inlined. This means one lambda gets become two inlined lambdas. What should happen when the left inlined lambda is applied to another argument than the right inline lambda? The superposition agents is responsible for superimposing the two differing arguments for a shared former lambda body.

![junction](diagrams/rendered/superposition.png){width=33%}

There are two superimposition ports. One left superimposed port and a one right superimposed port. The identification number for the agent symbol marks the order of the twin ports. The identification is always next to the left superimposition.

Having an unsymmetrical agent symbol helps with drawing net on paper. That way there can be less intersecting wires.

This superposition is not available in haskell. This leads to the phenomenon that computations inside lambdas are not shared.

# rule of inference: twin of lambda
A lambda needs to get duplicated. We need a new duplication node for the duplicated body and a superposition for keeping record of two differing arguments. Duplicating a lambda is easier to understand in the net representation.

![twin of lambda](diagrams/rendered/twin-of-lambda.png){width=66%}

First of all we get two inlined lambda agents. The lambda body is shared via a duplication agent. The variable in the lambda body is substituted by a superimposition of the the two new lambda variables.

The superposition agents is responsible for keeping track of the two possibly differing arguments that might get applied to the two lambdas.

A lambda being used means that they appear in an application agent and are evaluated. What happens when the inlined lambdas are used later on? The former lambda body that is now shared is shallow copied or inlined layer by layer on demand. Shared computations referenced in the lambda body are executed only once since they is no deep copy involved.

Note that the duplication family identification is used for the new duplication and superposition agent.

# rule of inference: application of superposition

It is possible to pass lambdas as arguments to a lambdas. Inside the outer lambda body there could be a superposition. This superposition might superimposes two lambdas. How to apply superimposed lambdas to an argument?

The application of a superposition is very easy to understand in the string notation:

`{f g} a` ↷ `{(f a) (g a)}` (the duplication node is implicit here)

The rule is simply the distributive law with ` ` instead of multiplication `·` and `{}` instead of addition `+`:

`(f {} g) · a` ↷ `(f · a) {} (g · a)`

![application of superposition](diagrams/rendered/application-of-superposition.png){width=66%}

An Application of a superposition becomes a superimposition of two applications. The applied arguments is duplicated because there are two applications now.

# rule of inference: rule on superposition
Rules on superpositions are handled similar to the application of superposition rule. All the children that are not of the considered superposition child are duplicated. The net representation appears more complicated than it is.

![rule of superposition](diagrams/rendered/rule-of-superposition.png){width=66%}

The `a` and `b` subgraphs are not copied.

# rule of inference: twin of superposition easy variant
What happens when a duplication agent duplicates a shared lambda body down to the (now superimposed) lambda variable? The order of the superimposed arguments tell us what the left and right argument was.

![twin of superposition easy](diagrams/rendered/twin-of-superposition-easy.png){width=66%}

Note that the the two resulting wires on the right are intersecting depending on the superimposition variants order of children.

# rule of inference: twin of superposition swap variant

There can be more than one duplication. When a superposition is duplicated the duplication might not come from the former duplication of the corresponding lambda of the superposition.

![twin of superposition swap](diagrams/rendered/twin-of-superposition-swap.png){width=66%}

The identification of the old duplication agent is carried over to the two new duplication agents. The identification of the old superposition is carried over to the two new superpositions.

# single threaded deterministic evaluation

How to actually implement an evaluation? There is so called weak-head-mormal-form. It is called like for lack of a better name. weak-head-mormal-form is an easy concept. It is difficult to describe but easy to define.

## weak-head-mormal-form
A redex `term` (reducible expression) is not in weak-head-mormal-form when the root agent is one of these:
```
application
duplication
rule
```
It is in weak-head-normal-form when:
```
literal
constructor
lambda
superposition
```

The embedder does not have to deal with these agents:
```
superposition
lambda variable
```

## algorithm

Now for the single threaded deterministic evaluation algorithm called `reduce`. Reduce does not mean that umber of agents or similar metrics necessarily go down. `reduce` reduces the number of remaining computational steps.

The input is some root. The output is some possible new root. In net representation it is the same wire into the runtime state.

In case the root agents is:
  - a literal: return
  - a constructor: return
  - a lambda: return
  - a superposition: return
  - an application:
    * apply application rule
    * `reduce` result again
  - a duplication:
    * apply on of the twin-of rules
    * `reduce` result again
  - a rule:
    * reduce all strict arguments from left to right
      * in case argument is superposition apply relevant rule; return
    * in case all strict arguments evaluated: apply the custom rule (perhaps with pattern matching)
    * `reduce` result again
  - a lambda variable
    * unreachable

With these ingredients the embedder can interact with the runtime state, by:
  - input: linking new agents
  - output: disintegrating constructors
  - composing: combining two roots with a new application node
  - drive: calling reduce to drive computation forward
