# net representation

[[_TOC_]]

## agents

### constructor
![hanger](diagrams/rendered/constructor.png){width=33%}
### literal
![box](diagrams/rendered/literal.png){width=33%}
### erasure
![dead-end](diagrams/rendered/erasure.png){width=33%}
### rule
![hacksaw](diagrams/rendered/rule.png){width=33%}

### lambda
![heart](diagrams/rendered/lambda.png){width=33%}
### application
![sickle](diagrams/rendered/application.png){width=33%}

### duplication
![slider](diagrams/rendered/duplication.png){width=33%}
### superposition
![junction](diagrams/rendered/superposition.png){width=33%}




## rules of inference

### erase

#### literal
![erase literal](diagrams/rendered/erase-literal.png)

#### constructor
![erase constructor](diagrams/rendered/erase-constructor.png)

#### twin
![erase twin](diagrams/rendered/erase-twin.png)



### application

#### lambda
![application of lambda](diagrams/rendered/application-of-lambda.png)

#### superposition
![application of superposition](diagrams/rendered/application-of-superposition.png)



### rule

#### intrinsic addition
![intrinsic rule of addition](diagrams/rendered/addition.png)

#### If
![rule that is If](diagrams/rendered/If.png)

#### superposition
![rule of superposition](diagrams/rendered/rule-of-superposition.png)



### twin

#### literal
![twin of literal](diagrams/rendered/twin-of-literal.png)

#### constructor
![twin of constructor](diagrams/rendered/twin-of-constructor.png)

#### lambda
![twin of lambda](diagrams/rendered/twin-of-lambda.png)

#### superposition easy variant
![twin of superposition easy](diagrams/rendered/twin-of-superposition-easy.png)

#### superposition swap variant
![twin of superposition swap](diagrams/rendered/twin-of-superposition-swap.png)



### disintegrate constructor
![disintegrate constructor](diagrams/rendered/disintegrate-constructor.png)