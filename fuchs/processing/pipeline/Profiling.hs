module Profiling where

import Data.Time.Clock
import Numeric.Natural
import Data.List
import System.Directory
import System.Process
import System.IO
import Control.Concurrent.Async

import Control.Concurrent.MVar

import AutomaticTesting

import Control.Concurrent
import Control.Monad



data Behaviour = Include | Exclude

negateBehaviour Include = Exclude
negateBehaviour Exclude = Include


knobs :: [Natural]
knobs = [0..]


standard_basis :: Natural -> Natural -> Behaviour
standard_basis
  the_one_one
  some_knob
  = if the_one_one == some_knob
      then Include
      else Exclude


data Profil = Profil [Behaviour]

mapProfil f (Profil x) = Profil (fmap f x)

cutProfileHere :: Natural -> Profil -> Profil
cutProfileHere n (Profil x) = Profil $ genericTake n $ x

all_but_one :: Natural -> Profil
all_but_one knob = result
  where
    result = Profil basis_vector

    basis_vector = map (standard_basis knob) knobs

all_activated :: Profil
all_activated
  = id
  $ Profil
  $ map (const Include)
  $ knobs

compileProfil (Profil list)
  = SerializedProfile result
  where
    result
      = unlines
      $ map encode list

    encode Include = "i"
    encode Exclude = "e"


profiling_test
  number_of_knobs
  n
  = id
  $ compileProfil
  $ cutProfileHere number_of_knobs
  $ mapProfil negateBehaviour
  $ all_but_one
  $ n

ground_zero_profiling_test :: Natural -> SerializedProfile
ground_zero_profiling_test
  number_of_knobs
  = id
  $ compileProfil
  $ cutProfileHere number_of_knobs
  $ all_activated

allexcluded_profiling_test :: Natural -> SerializedProfile
allexcluded_profiling_test
  number_of_knobs
  = id
  $ compileProfil
  $ cutProfileHere number_of_knobs
  $ mapProfil negateBehaviour
  $ all_activated

start_webserver :: MVar () -> MVar () -> MVar () -> MVar () -> IO String
start_webserver
  webserver_is_ready
  messuarement_finished
  websever_was_sucessfully_shutdown
  foreverWaitForNothing
  = do

  (_, Just hout, Just herror, processHandle)
    <- createProcess
          (proc "sh" ["develop-no-internet.sh"])
          { std_out = CreatePipe
          , std_err = CreatePipe
          , delegate_ctlc = True
          , create_group = True
          }

  output <- hGetContents hout
  output_herror <- hGetContents herror

  let outputLines = lines output

  eitherFound <- checkForMagicLine outputLines

  concurrently_
    -- consume all the output, maybe that helps
    (concurrently_
      (do
        threadDelay 20000
        putStrLn $ (show $ length output) ++ show ("length output", length output)
        threadDelay 20000
        putStrLn output
        threadDelay 2000
      )
      (do
        threadDelay 700000
        putStrLn $ (show $ length output_herror) ++ show ("length output_herror", length output_herror)
        threadDelay 700000
        putStrLn output_herror
        threadDelay 700000
      )
    )
    $ do
    threadDelay 70000

--     TODO this leads to a webpage with just the html text: "Something went wrong" !?!?!? WTF
    case eitherFound of
      Right () -> do
        threadDelay 70000
        print "found magic output"
      Left () -> do
        print "end of output but no magic output:"
        print "########################"
        putStrLn output
        print "########################"
        putStrLn output_herror
        print "########################"
        maybeexitcode <- getProcessExitCode processHandle
        print ("maybeexitcode", maybeexitcode)
        error $ "did not find magic value that indicates that the webserver is ready"

    threadDelay 2000
    print "now wait for meassurement"

    takeMVar messuarement_finished

    threadDelay 3000
    print "got signal messuarement_finished"

    -- TODO terminate does not work as children are not killed as well
  --   () <- terminateProcess processHandle
    -- TODO this is utter bullshit
    () <- interruptProcessGroupOf processHandle

    maybeexitcode <- getProcessExitCode processHandle

    threadDelay 3000
    print ("maybe exitcode", maybeexitcode)

    exitcode <- waitForProcess processHandle

    threadDelay 3000
    print ("exitcode", exitcode)

    threadDelay 3000
    print "now wait for race"
    threadDelay 3000
    putMVar websever_was_sucessfully_shutdown ()

    takeMVar foreverWaitForNothing

  return "wetting pants"

  where
    checkForMagicLine :: [String] -> IO (Either () ())
    checkForMagicLine []
      = return $ Left ()
    checkForMagicLine ("1f832d4a7b575aa8a179265a595df410f105b2":xs)
      = do
      putMVar webserver_is_ready ()
      return $ Right ()
    checkForMagicLine (_:xs)
      = checkForMagicLine xs



do_one_messurement
  webserver_is_ready
  messuarement_finished
  websever_was_sucessfully_shutdown
  = do
  takeMVar webserver_is_ready
  time <- measure_time
  putMVar messuarement_finished ()
  threadDelay 100
  print "wait for webserver to be shutdown"
  takeMVar websever_was_sucessfully_shutdown
  return time







tune_one_knob_of_many
  :: Natural
  -> Natural
  -> IO NominalDiffTime
tune_one_knob_of_many
  number_of_knobs
  knob
  = do
  let serialiedProfile = profiling_test number_of_knobs knob

  measure_for_profile serialiedProfile


data SerializedProfile = SerializedProfile String deriving Show


injectProfile
  (SerializedProfile serialiedProfile)
  = writeFile "stonefish/stonefish/processing/source/profiling_hints" serialiedProfile

printProfile
  (SerializedProfile serialiedProfile)
  = print $ filter (/= '\n') serialiedProfile

measure_for_profile :: SerializedProfile -> IO NominalDiffTime
measure_for_profile
  serialiedProfile
  = do
  injectProfile serialiedProfile

  printProfile serialiedProfile

  webserver_is_ready <- newEmptyMVar :: IO (MVar ())
  messuarement_finished <- newEmptyMVar :: IO (MVar ())
  websever_was_sucessfully_shutdown <- newEmptyMVar :: IO (MVar ())
  foreverWaitForNothing <- newEmptyMVar :: IO (MVar ())


  eitherResult
    <- race
          (start_webserver
            webserver_is_ready
            messuarement_finished
            websever_was_sucessfully_shutdown
            foreverWaitForNothing
          )
          (do_one_messurement
            webserver_is_ready
            messuarement_finished
            websever_was_sucessfully_shutdown
          )

  time
    <- case eitherResult of
        Right time
          -> return time
        Left webserver_pants_wetting
          -> error
              $ "webserver_pants_wetting: "
              ++ webserver_pants_wetting

  print ("measured time: ", time)

  return time




stonefish_profiling = do
  putStrLn "profiling"

  number_of_knobs_string <- readFile "stonefish/stonefish/processing/result/stonefishRuntime_Explore/number_of_performance_knobs"

  let number_of_knobs = (read number_of_knobs_string :: Natural)



  let tune_one_knob = tune_one_knob_of_many number_of_knobs

  let allKnobs
        = id
--         $ take 3
--         $ drop 100
        $ [0..number_of_knobs-1]

  results <- mapM tune_one_knob allKnobs

  putStrLn ""
  putStrLn ""


  print "do ground zero test"
  ground_zero_time
    <- measure_for_profile
    $ ground_zero_profiling_test
    $ number_of_knobs
  print ("ground_zero_time", ground_zero_time)

  print "all_excluded_time"
  all_excluded_time
    <- measure_for_profile
    $ allexcluded_profiling_test
    $ number_of_knobs
  print ("all_excluded_time", all_excluded_time)


  putStrLn ""
  putStrLn ""


  print ("ground_zero_time", ground_zero_time)
  print ("all_excluded_time", all_excluded_time)

  print results

  let timedifferences = map (\x -> x-ground_zero_time) results

  putStrLn "timedifferences:"
  print $ timedifferences
  putStrLn ""

  let profil = map interpret timedifferences

  let result_profile = compileProfil $ Profil profil

  putStrLn "result_profile:"
  printProfile $ result_profile
  putStrLn ""

  injectProfile $ result_profile

  measure_for_profile result_profile

  print "end result"
  printProfile $ compileProfil $ Profil profil
  return ()

interpret x = if x < 0 then Exclude else Include
