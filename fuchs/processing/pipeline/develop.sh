set -e



WANT_EVERYTHING_COMMITED=$1
NO_INTERNET_NIX_FUCKUP=$2
NO_RUN_JUST_BUILT=$3
SHOULD_DO_DEPENDENCY_UPDATES=$4


#

ERROR_GIT_REPOSITORY_NOT_COMMITTED=1

#

if [ -z "$(ls -A assembler)" ]; then
  echo "assembler subrepositiry is empty? did you forget to provide --recursive while cloning?"
  exit 1
else
  :
fi


#

echo "develop script"


function git_status ()
{
  git status --porcelain=v1 2>/dev/null | wc -l
}

function is_git_status_clean ()
{
  if [[ "$WANT_EVERYTHING_COMMITED" == "everything-is-commited" ]]; then
    echo "check commit status"

    if [[ $(git_status) -eq "0" ]]; then
      echo "everything is committed"
    else
      git status
      echo ""
      pwd
      exit $ERROR_GIT_REPOSITORY_NOT_COMMITTED
    fi
  fi
}


echo "compile stonefish runtime memory initialisation code"
pushd assembler/assembler
  is_git_status_clean
  ./build.sh $SHOULD_DO_DEPENDENCY_UPDATES $NO_INTERNET_NIX_FUCKUP
popd





echo "compile stonefish runtime"

rsync \
  --partial \
  --progress \
  --recursive \
  --delete \
  --checksum \
  assembler/assembler/processing/result/webassemblyInitialisationCode/memoryInitialisation.wat \
  stonefish/stonefish/processing/source/memoryInitialisation.wat

pushd stonefish/stonefish
  is_git_status_clean
  ./build.sh $SHOULD_DO_DEPENDENCY_UPDATES $NO_INTERNET_NIX_FUCKUP
popd





echo "compile web embedding"

rsync \
  --partial \
  --progress \
  --recursive \
  --delete \
  --checksum \
  stonefish/stonefish/processing/result/stonefishRuntime_WASM/ \
  web-embedding/web-embedding/processing/stonefishRuntime_WASM/

rsync \
  --partial \
  --progress \
  --recursive \
  --delete \
  --checksum \
  assembler/assembler/processing/result/assembler_libraries/ \
  web-embedding/web-embedding/processing/assembler_libraries/


pushd web-embedding/web-embedding
  is_git_status_clean
  ./build.sh $SHOULD_DO_DEPENDENCY_UPDATES $NO_INTERNET_NIX_FUCKUP
popd




echo "compiling godot extension"

rsync \
  --partial \
  --progress \
  --recursive \
  --delete \
  --checksum \
  assembler/assembler/processing/result/assembler_libraries/ \
  godot-embedding/godot-embedding/processing/assembler_libraries/

rsync \
  --partial \
  --progress \
  --recursive \
  --delete \
  --checksum \
  stonefish/stonefish/processing/result/stonefishRuntime_C/ \
  godot-embedding/godot-embedding/processing/stonefishRuntime_C/


rsync \
  --partial \
  --progress \
  --recursive \
  --delete \
  --checksum \
  assembler/assembler/processing/result/embeddingDevelopCase/pointcloud_filter_root.inc \
  godot-embedding/godot-embedding/processing/source/extension/pointcloud_filter_root.inc
rsync \
  --partial \
  --progress \
  --recursive \
  --delete \
  --checksum \
  assembler/assembler/processing/result/embeddingDevelopCase/pointcloud_filter_melee.inc \
  godot-embedding/godot-embedding/processing/source/extension/pointcloud_filter_melee.inc


rm -f godot-embedding/godot-embedding/processing/source/curvedtrack/examples/curve_simple.read.fuchs
rm -f godot-embedding/godot-embedding/processing/source/curvedtrack/examples/curve_slightly_more_complicated.read.fuchs
rm -f godot-embedding/godot-embedding/processing/source/curvedtrack/examples/perle_cube.read.fuchs
rm -f godot-embedding/godot-embedding/processing/source/curvedtrack/examples/perle_pennant.read.fuchs
rm godot-embedding/godot-embedding/processing/source/curvedtrack/examples/*.read.fuchs || true

rsync \
  --partial \
  --progress \
  --recursive \
  --delete \
  --checksum \
  assembler/assembler/processing/result/embeddingDevelopCase/curve_simple.read.fuchs \
  godot-embedding/godot-embedding/processing/source/curvedtrack/examples/curve_simple.read.fuchs

rsync \
  --partial \
  --progress \
  --recursive \
  --delete \
  --checksum \
  assembler/assembler/processing/result/embeddingDevelopCase/perle_pennant.read.fuchs \
  godot-embedding/godot-embedding/processing/source/curvedtrack/examples/perle_pennant.read.fuchs
rsync \
  --partial \
  --progress \
  --recursive \
  --delete \
  --checksum \
  assembler/assembler/processing/result/embeddingDevelopCase/perle_cube.read.fuchs \
  godot-embedding/godot-embedding/processing/source/curvedtrack/examples/perle_cube.read.fuchs
rsync \
  --partial \
  --progress \
  --recursive \
  --delete \
  --checksum \
  assembler/assembler/processing/result/embeddingDevelopCase/curve_slightly_more_complicated.read.fuchs \
  godot-embedding/godot-embedding/processing/source/curvedtrack/examples/curve_slightly_more_complicated.read.fuchs

pushd godot-embedding/godot-embedding
  pushd processing
    ./do.sh
  popd
  is_git_status_clean
popd






if [[ "$NO_RUN_JUST_BUILT" == "no-run" ]]; then
  notify-send "success" & true
  exit 0
fi






# TODO how to do such things correctly?
depelopFUSE="/tmp/depelopFUSE"
mkdir $depelopFUSE -p

cp --no-preserve=mode,ownership web-embedding/web-embedding/processing/result/binaries/develop_application $depelopFUSE/

chmod u+x $depelopFUSE/develop_application


cp --no-preserve=mode,ownership web-embedding/web-embedding/processing/result/binaries/email-backed-accounts_test-accounts.db3 $depelopFUSE/email-backed-accounts.db3





echo "READY FOR PROFILING"
echo "1f832d4a7b575aa8a179265a595df410f105b2"
echo "READY FOR PROFILING"


echo "starting local server at: localhost:3000"
echo ">>>>>>>>>>>>>>>>>>>>>>>>>>       <<<<<<<<<<<<<<<<<<<<<<<<<<"
echo ">>>>>>>>>>>>>>>>>>>>>>>>>> READY <<<<<<<<<<<<<<<<<<<<<<<<<<"
echo ">>>>>>>>>>>>>>>>>>>>>>>>>>       <<<<<<<<<<<<<<<<<<<<<<<<<<"

pushd $depelopFUSE

  #TODO Why do i have to do this? Why is this not standard behaviour?
  trap "trap - SIGTERM && kill -kill 0" SIGINT SIGTERM EXIT

  notify-send "go" & true
  ./develop_application
popd
