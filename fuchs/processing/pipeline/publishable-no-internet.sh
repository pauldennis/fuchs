set -e

./develop.sh \
  "" \
  no-internet \
  no-run \
  no-dependency-update


./develop.sh \
  everything-is-commited \
  no-internet \
  run \
  no-dependency-update
