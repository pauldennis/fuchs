{ sources ? import ./nix/sources.nix
, pkgs ? import sources.nixpkgs {}
, glibcLocales ? pkgs.glibcLocales

, ghc ? import ./glasgowHaskellCompiler.nix {}

, rsync ? pkgs.rsync
, nix ? pkgs.nix
, eza ? pkgs.eza
, libnotify ? pkgs.libnotify
, git ? pkgs.git
, cacert ? pkgs.cacert
, vscodium ? pkgs.vscodium
, tldr ? pkgs.tldr
, hexyl ? pkgs.hexyl
, htop ? pkgs.htop
, signal-desktop ? pkgs.signal-desktop
, scons ? pkgs.scons
, inkscape ? pkgs.inkscape
}:

pkgs.mkShell {
  buildInputs = [
    cacert

    nix
    rsync
    libnotify
    git

    scons
    ghc

    tldr
    hexyl
    htop
    eza

    vscodium
    signal-desktop
    inkscape
  ];

  shellHook = ''
    echo this is a nix shell; alias l="ls -al"
    cd pipeline
  '';

  # excruciating unicode and whatnot
  LOCALE_ARCHIVE="${pkgs.glibcLocales}/lib/locale/locale-archive";
  LANG="en_GB.UTF8";

  MY_ENVIRONMENT_VARIABLE = "world";
}
