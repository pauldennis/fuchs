{ sources ? import ./nix/sources.nix
, pkgs ? import sources.nixpkgs_old {}

, ghc ? pkgs.haskellPackages.ghcWithPackages (pkgs: with pkgs; [
    webdriver
  ])

}:

ghc
